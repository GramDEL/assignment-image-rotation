#include "bmp.h"
#include <stdbool.h>
#include <malloc.h>
#define BM 19778
#define BMINFOHEADER 40

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE* f, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

static bool write_header(FILE* f, struct bmp_header* header) {
    return fwrite(header, sizeof(struct bmp_header), 1, f);
}

static uint32_t calc_padding(const uint32_t w) {
    return (4 - ((3 * w) % 4)) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    read_header(in, &header);

    if (header.biWidth <= 0 || header.biHeight <= 0 || header.bfileSize == 0) {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    uint32_t x = header.biWidth, y = header.biHeight, amount_of_bits = 0;
    struct pixel* data = malloc(x * y * sizeof(struct pixel));
    fseek(in, header.bOffBits, SEEK_SET);
    for (uint32_t i = 0; i < y; i++) {
        amount_of_bits += fread(data + i * x, sizeof(struct pixel), x, in);
        fseek(in, calc_padding(x), SEEK_CUR);
    }
    if (amount_of_bits * sizeof(struct pixel) != header.bfileSize - header.bOffBits - y * calc_padding(x)) {
        free(data);
        return READ_CORRUPTED_BMP;
    }
    *img = create_image(x, y, data);
    return READ_OK;
}

static size_t to_bmp_write(FILE* out, struct image const* img) {
    size_t amount_of_bits = 0;
    const char end = 0;
    for (uint64_t i = 0; i < img->height; i++) {
        amount_of_bits += fwrite(img->data + i * img->width, sizeof(char), img->width * 3, out);
        amount_of_bits += fwrite(&end, sizeof(char), calc_padding(img->width), out);
    }
    return amount_of_bits;
}

struct bmp_header create_header(struct image const* img) {
    struct bmp_header header = {0};
    header.bfType = BM;
    header.biSize = BMINFOHEADER;
    header.biPlanes = 1;
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biBitCount = 24;
    header.bOffBits = sizeof(struct bmp_header);
    header.bfileSize = header.bOffBits + img->height * (img->width * 3 + calc_padding(img->width));
    return header;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_header(img);
    write_header(out, &header);
    if (to_bmp_write(out, img) != header.bfileSize - sizeof(struct bmp_header)) {
        return WRITE_CORRUPTED_IMAGE;
    }
    return WRITE_OK;
}
