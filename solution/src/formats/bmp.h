#ifndef BMP_H
#define BMP_H

#include "../image/image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_CORRUPTED_BMP
};

enum write_status {
    WRITE_OK = 0,
    WRITE_CORRUPTED_IMAGE
};

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif
