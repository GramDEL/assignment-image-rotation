#include "image.h"
#include <malloc.h>

struct image init_image() {
    return (struct image) {0};
}

struct image create_image(uint64_t w, uint64_t h, struct pixel* const data) {
    return (struct image) {.width = w, .height = h, .data = data};
}

void destroy_image(struct image* img) {
    if (img->data != NULL) {
        free(img->data);
    }
}
