#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>

struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image init_image();
struct image create_image(uint64_t w, uint64_t h, struct pixel* const data);
void destroy_image(struct image* img);

#endif
