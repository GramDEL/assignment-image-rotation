#include <stdio.h>
#include "formats/bmp.h"
#include "utils/error.h"
#include "utils/files.h"
#include "transformations/rotate.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        print_err("Not enough arguments!");
    } else if (argc > 3) {
        print_err("Too many arguments!");
    } else {
        struct image old_img = init_image(), new_img = init_image();

        switch (file_load(argv[1], &old_img, from_bmp)) {
            case LOAD_OK:
                break;
            case LOAD_WRONG_FORMAT:
                print_err("Wrong source image file format!");
                goto free;
            case LOAD_NO_SUCH_FILE_OR_DIRECTORY:
                print_err("Wrong source image: no such file or dir!");
                goto free;
        }
        new_img = rotate_image(old_img);
        switch (file_save(argv[2], &new_img, to_bmp)) {
            case SAVE_OK:
                break;
            case SAVE_WRONG_FORMAT:
                print_err("Wrong transformed image file format!");
                goto free;
            case SAVE_NO_SUCH_FILE_OR_DIRECTORY:
                print_err("Wrong transformed image: no such file or dir!");
                goto free;
        }

        free:
        destroy_image(&old_img);
        destroy_image(&new_img);
    }
    return 0;
}
