#include "rotate.h"
#include <malloc.h>

struct image rotate_image(const struct image img) {
    struct image new_img;
    new_img.width = img.height;
    new_img.height = img.width;
    new_img.data = malloc(new_img.width * new_img.height * sizeof(struct pixel));

    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            size_t new_index = (img.width - j - 1) * img.height + i;
            new_img.data[new_index] = img.data[i * img.width + j];
        }
    }
    return new_img;
}
