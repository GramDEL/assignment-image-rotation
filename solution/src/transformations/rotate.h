#ifndef ROTATE_H
#define ROTATE_H

#include "../image/image.h"

struct image rotate_image(const struct image img);

#endif
