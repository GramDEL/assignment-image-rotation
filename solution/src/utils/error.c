#include "error.h"
#include <stdarg.h>
#include <stdio.h>

void print_err(const char* msg, ...) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    vfprintf(stderr, "\n", args);
    va_end (args);
}
