#include "files.h"

enum load_status file_load(const char* filename, struct image* img, enum read_status fun(FILE*, struct image*)) {
    FILE* f;
    f = fopen(filename, "rb");
    if (f != NULL) {
        switch (fun(f, img)) {
            case READ_OK:
                fclose(f);
                return LOAD_OK;
            case READ_INVALID_HEADER:
                fclose(f);
                print_err("Invalid bmp header!");
                return LOAD_WRONG_FORMAT;
            case READ_INVALID_BITS:
                fclose(f);
                print_err("Invalid bits!");
                return LOAD_WRONG_FORMAT;
            case READ_CORRUPTED_BMP:
                fclose(f);
                print_err("Corrupted file!");
                return LOAD_WRONG_FORMAT;
        }
    }
    return LOAD_NO_SUCH_FILE_OR_DIRECTORY;
}

enum save_status file_save(const char* filename, struct image const* img, enum write_status fun(FILE*, struct image const*)) {
    FILE* f;
    f = fopen(filename, "wb");
    if (f != NULL) {
        switch (fun(f, img)) {
            case WRITE_OK:
                fclose(f);
                return SAVE_OK;
            case WRITE_CORRUPTED_IMAGE:
                print_err("Invalid header or image!");
                return SAVE_WRONG_FORMAT;
        }
    }
    return SAVE_NO_SUCH_FILE_OR_DIRECTORY;
}
