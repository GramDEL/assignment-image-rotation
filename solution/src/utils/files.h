#ifndef FILES_H
#define FILES_H

#include "../formats/bmp.h"
#include "../image/image.h"
#include "error.h"

enum load_status {
    LOAD_OK = 0,
    LOAD_WRONG_FORMAT,
    LOAD_NO_SUCH_FILE_OR_DIRECTORY
};

enum save_status {
    SAVE_OK = 0,
    SAVE_WRONG_FORMAT,
    SAVE_NO_SUCH_FILE_OR_DIRECTORY
};

enum load_status file_load(const char* filename, struct image* img, enum read_status fun(FILE*, struct image*));
enum save_status file_save(const char* filename, struct image const* img, enum write_status fun(FILE*, struct image const*));

#endif
